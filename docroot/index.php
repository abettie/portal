<?php
	$link_info = array(
		array(
			"id" => "link01",  // javascriptで遷移させるためのid
			"url" => "http://enchantjs.makedara.net/",
			"title" => "enchant.js GAME",
			"color" => "white",
			"bgcolor" => "#369CBA",
			"icon" => "gamepad",  // font-awesomeを表示するためのkey
		),
		array(
			"id" => "link02",
			"url" => "http://beacchi.makedara.net/",
			"title" => "Beacchi",
			"color" => "white",
			"bgcolor" => "#C1272D",
			"icon" => "paw",
		),
		array(
			"id" => "link03",
			"url" => "http://aipo.makedara.net/",
			"title" => "Aipo",
			"color" => "white",
			"bgcolor" => "#F98A45",
			"icon" => "calendar",
		),
		array(
			"id" => "link04",
			"url" => "https://babywords.makedara.net/",
			"title" => "Baby Words",
			"color" => "white",
			"bgcolor" => "limegreen",
			"icon" => "smile-o",
		),
	);
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5ZLVBW5');</script>
<!-- End Google Tag Manager -->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<script src="js/jquery-2.1.4.min.js"></script>
	<link rel="stylesheet" href="css/index.css" media="all">
	<title>Makedara Portal</title>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5ZLVBW5"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="title">
				Makedara Portal
			</div>
		</div>
<?php foreach ($link_info as $l) { ?>
		<div class="col-xs-12 col-sm-12 col-md-6">
			<div class="link" id="<?= $l["id"] ?>" style="color: <?= $l["color"] ?>; background-color:<?= $l["bgcolor"] ?>;">
				<i class="fa fa-<?= $l["icon"] ?>"></i><span><?= $l["title"] ?></span>
			</div>
		</div>
<?php } ?>
	</div>
</div>

<script type="text/javascript">
//<![CDATA[
$(function() {
<?php foreach ($link_info as $l) { ?>
	// クリックしたら遷移
	$("#<?= $l["id"] ?>").click(function() {
		window.location.href = "<?= $l["url"] ?>";
	});
<?php } ?>
});
//]]>
</script>
</body>
</html>
